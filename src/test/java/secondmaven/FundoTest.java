package secondmaven;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

import main.java.secondmaven.Fundo;

public class FundoTest {

	@Test
	public void deve_retornar_10_se_valor_maior_que_zero() {
		Fundo fundo = new Fundo();
		double valorRetornado = fundo.validarValor(1D);
		Assert.assertEquals("ERRO", 10D, valorRetornado, 0);
	}
	
	@Test
	public void deve_multiplicar_se_valor_menorigual_zero() {
		Fundo fundo = new Fundo();
		Double valorRetornado = fundo.validarValor(0D);
		
		assertEquals(0, valorRetornado, 0);
	}
	
	
	@Test
	public void deve_retornar_zero_se_nulo() {
		Fundo fundo = new Fundo();
		Double valorRetornado = fundo.validarValor(null);
		
		assertEquals(0, valorRetornado, 0);
	}

}
